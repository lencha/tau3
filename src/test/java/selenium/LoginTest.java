package selenium;

import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.StaleElementReferenceException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.testng.Assert;
import org.testng.annotations.Test;

public class LoginTest extends BaseTest {

    @Test
    public void invalidLoginWebex() {
        String url = webDriver.getCurrentUrl();
        Assert.assertEquals(url, WEBEX);
        WebElement loginButton = webDriver.findElement(By.id("guest_signin_button"));
        loginButton.click();

        WebElement emailTextInput = wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//input[@class='md-input']")));
        emailTextInput.sendKeys("123");
        WebElement nextButton = webDriver.findElement(By.id("IDButton2"));
        nextButton.click();

        WebElement errorMessage = wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("nameContextualError1")));
        Assert.assertTrue(checkPresenceOfErrorMessage(errorMessage), "No Error message");
        Assert.assertEquals(errorMessage.getText(), ERROR_MESSAGE);
    }

    @Test
    public void loginWebex() {
        Assert.assertEquals(webDriver.getCurrentUrl(), WEBEX);

        WebElement loginButton = webDriver.findElement(By.id("guest_signin_button"));
        loginButton.click();
        WebElement emailTextInput = wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("IDToken1")));
        emailTextInput.sendKeys(EMAIL);
        WebElement nextButton = webDriver.findElement(By.id("IDButton2"));
        nextButton.click();
        WebElement errorMessage = webDriver.findElement(By.id("nameContextualError1"));
        Assert.assertFalse(checkPresenceOfErrorMessage(errorMessage), "Error message");

        WebElement myPortal = wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[text()[contains(., 'MyPortal Account (Alternative)')]]")));
        myPortal.click();
        WebElement userId = wait.until(ExpectedConditions.visibilityOfElementLocated(By.name("userId")));
        userId.sendKeys(EMAIL);
        WebElement userPassword = wait.until(ExpectedConditions.visibilityOfElementLocated(By.name("password")));
        userPassword.sendKeys(PASSWORD);
        WebElement ciamLoginButton = webDriver.findElement(By.id("submit"));
        ciamLoginButton.click();

        Assert.assertEquals(webDriver.getCurrentUrl(), WEBEX_PERSONAL_ROOM);
        WebElement personalRoomCardTitle = wait.until(ExpectedConditions.visibilityOfElementLocated(By.className("pmr_card_c_title")));
        Assert.assertEquals(personalRoomCardTitle.getText(), CARD_TITLE);
    }

    public boolean checkPresenceOfErrorMessage(WebElement errorMessage) {
        try {
            return errorMessage.isDisplayed();
        } catch (StaleElementReferenceException | NoSuchElementException elementHasDisappeared) {
            return false;
        }
    }
}
