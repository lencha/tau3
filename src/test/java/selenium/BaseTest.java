package selenium;

import io.github.bonigarcia.wdm.WebDriverManager;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;

public class BaseTest {
    protected WebDriver webDriver;
    protected WebDriverWait wait;
    protected static final String WEBEX = "https://dtag.webex.com/webappng/sites/dtag/dashboard?siteurl=dtag";
    protected static final String ERROR_MESSAGE = "Enter a valid email address. Example: name@email.com";
    protected static final String EMAIL = "elena.rymar@t-systems.com";
    protected static final String PASSWORD = "***";
    protected static final String WEBEX_PERSONAL_ROOM = "https://dtag.webex.com/webappng/sites/dtag/dashboard?siteurl=dtag&from_login=true";
    protected static final String CARD_TITLE = "Elena Rymar's Personal Room";

    @BeforeClass
    public void init(){
        WebDriverManager.chromedriver().setup();
        webDriver = new ChromeDriver();
        webDriver.get(WEBEX);
        wait = new WebDriverWait(webDriver, 15);
    }

    @AfterMethod
    public void tearDown() {
        if (webDriver != null) {
            webDriver.quit();
        }
    }
}
