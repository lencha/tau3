package selenide;

import com.codeborne.selenide.Browsers;
import com.codeborne.selenide.Configuration;
import com.codeborne.selenide.Selenide;
import org.openqa.selenium.WebDriver;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;

public class BaseTest {
    protected WebDriver webDriver;
    protected static final String WEBEX = "https://dtag.webex.com/webappng/sites/dtag/dashboard?siteurl=dtag";
    protected static final String ERROR_MESSAGE = "Enter a valid email address. Example: name@email.com";
    protected static final String WEBEX_PERSONAL_ROOM = "https://dtag.webex.com/webappng/sites/dtag/dashboard?siteurl=dtag&from_login=true";
    protected static final String CARD_TITLE = "Personal Room";

    @BeforeClass
    public void init(){
        Configuration.browser = Browsers.CHROME;
        Configuration.timeout = 7500;
        Configuration.baseUrl = WEBEX;
        Configuration.startMaximized = true;
        Configuration.headless = false;

        Selenide.open(WEBEX);
    }

    @AfterMethod
    public void tearDown() {
        if (webDriver != null) {
            Selenide.closeWindow();
        }
    }
}
