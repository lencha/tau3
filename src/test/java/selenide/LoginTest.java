package selenide;

import com.codeborne.selenide.Condition;
import com.codeborne.selenide.WebDriverRunner;
import org.openqa.selenium.By;
import org.testng.Assert;
import org.testng.annotations.Test;
import utils.PropertyLoader;

import static com.codeborne.selenide.Selenide.*;
import static utils.PropertyLoader.LOGIN_FILE;

public class LoginTest extends BaseTest {

    @Test
    public void invalidLoginWebex() {
        $(By.id("guest_signin_button")).click();
        $x("//input[@class='md-input']").val("123");
        $(By.id("IDButton2")).click();
        $(By.id("nameContextualError1")).shouldBe(Condition.visible)
                .shouldHave(Condition.text(ERROR_MESSAGE));
    }

    @Test
    public void loginWebex() {
        $(By.id("guest_signin_button")).click();
        $(By.id("IDToken1")).val(PropertyLoader.loadProperty("email", LOGIN_FILE));
        $(By.id("IDButton2")).click();
        $(By.id("nameContextualError1")).shouldNotBe(Condition.visible);
        $$(By.id("nameContextualError1")).shouldHaveSize(0);
        $x("//*[text()[contains(., 'MyPortal Account (Alternative)')]]").click();
        $(By.name("userId")).val(PropertyLoader.loadProperty("email", LOGIN_FILE));
        $(By.name("password")).val(PropertyLoader.loadProperty("password", LOGIN_FILE));
        $(By.name("login")).click();

        Assert.assertEquals(WebDriverRunner.url(), WEBEX_PERSONAL_ROOM);
        $(By.className("pmr_card_c_title")).shouldHave(Condition.text(CARD_TITLE));
    }
}
